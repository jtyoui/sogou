# **sogou** [![tyoui](https://github.com/zhangwei0530/logo/blob/master/logo/photolog.png?raw=true)](http://www.tyoui.cn)

[![](https://github.com/zhangwei0530/logo/blob/master/logo/logo.png?raw=true)](http://www.tyoui.cn)

## 这个一个搜狗词库爬虫，只需要一步就能爬完搜狗上的所有词库
[![](https://img.shields.io/badge/java-IDEA-yellow.svg)]()
[![](https://img.shields.io/badge/jdk-8-green.svg)]()
[![](https://img.shields.io/badge/BlogWeb-Tyoui-bule.svg)](http://www.tyoui.cn)
[![](https://img.shields.io/badge/Email-tyoui@tyoui.cn-red.svg)]()

## 使用maven下载
    <!-- https://mvnrepository.com/artifact/cn.tyoui/sogou -->
    <dependency>
        <groupId>cn.tyoui</groupId>
        <artifactId>sogou</artifactId>
        <version>1.8.3</version>
    </dependency>


## 执行程序
        public static void main(String[] args) throws Exception {
              SoGou soGou = new SoGou();
              //下载搜狗词库所有url地址。并保存在本项目下的sogou.txt文件中
              //soGou.download_sogou();
              //讲文本下的url地址下载到D盘下的cell文件夹中
              //soGou.download_url("D://cell");
              //将搜狗文件转化成txt文本
              soGou.toTxt("D:\\cell\\89个节日.scel", "D:\\cell\\89个节日.txt", false);
        }

## 下载如图
soGou.download_sogou();
![](https://github.com/zhangwei0530/logo/blob/master/photo/sogou.png?raw=true)


## 地址和名字也\t分割
![](https://github.com/zhangwei0530/logo/blob/master/photo/sogou1.png?raw=true)

## 下载在本机如图
![](https://github.com/zhangwei0530/logo/blob/master/photo/sogou2.png?raw=true)

## 下载在D盘下cell目录
soGou.download_url("D://cell");
![](https://github.com/zhangwei0530/logo/blob/master/photo/sogou3.jpg?raw=true)

#################################################################################

# 转换文本有两种方式，选其中一种即可


## 第一种:(软件转换)在本项目下有一个搜狗文件转换器。可以将scel文件装换成純文本文件
![](https://github.com/zhangwei0530/logo/blob/master/photo/sogou4.png?raw=true)
![](https://github.com/zhangwei0530/logo/blob/master/photo/sogou5.png?raw=true)
搜狗文件是二进制，必须要转成txt才能看见

## 第二种:(代码转换)在D盘下的scel文件转化成txt
    soGou.toTxt("D:\\cell\\89个节日.scel", "D:\\cell\\89个节日.txt", false);
    第一个参数是scel文件的地址
    第二参数是保存txt位置
    第三参数是是否追加 true是追加 false是不追加